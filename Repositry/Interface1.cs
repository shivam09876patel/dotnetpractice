﻿using consolecoreapp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolecoreapp.Repositry
{
    internal interface UserInterface
    {
        int checkInterface(int value1);
        User adduser(User user);
        User[] getAllUsers();
        User GetUserById(int userid);
        User GetUserByNmae(User user, string name);
    }
}
